/**
 * Created by matthew on 03/08/17.
 */
var http  = require( "http" ),
	url = require( "url" );

http.createServer( function( request, response ) {

	var codes = url.parse( request.url ), data, dataJson;

	if( codes.pathname === "/" ) {
		data = { count: 101 };
	}
	else if( codes.pathname === "/two" ) {
		data = { count: 2020 };
	}

	dataJson = JSON.stringify(data);

	setTimeout( function() {
		response.writeHead(200, {
			'Content-Type' : 'application/json'
		});
		response.end(dataJson);
	}, 1000 );


} ).listen(40080);