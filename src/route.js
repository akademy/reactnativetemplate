import React from 'react';
import { StackNavigator, NavigationActions } from 'react-navigation';

import Home from "./screens/bind/home"
import About from "./screens/about"
import Count from "./screens/bind/count"

import { NAVIGATION as N } from './constants'

export const changePathAndNavigate = ( navigation, routeList, index ) => {
	/*
	Change the StackNavigation's stack, to some other combination of screens
	e.g. changePathAndNavigate( this.props.navigation, [N.HOME, N.QUESTIONS] );
	 */
	let actions = [];
	for( let i=0, z=routeList.length; i<z; i++ ) {
		actions.push( NavigationActions.navigate({ routeName: routeList[i] }), )
	}
	const resetAction = NavigationActions.reset({
		index: (index !== undefined) ? index : actions.length-1,
		actions
	});
	navigation.dispatch(resetAction);
};

const routerSetup = {};
routerSetup[N.HOME] = {
	screen: Home,
	//navigationOptions: ({navigation, screenProps}) => ({
	//	title: `${screenProps.build} Home (StackNavigator)`,
	//}),
};
routerSetup[N.COUNT] = {
	screen: Count
};
routerSetup[N.ABOUT] = {
	screen: About
};

// Main Navigation
const Router = StackNavigator( routerSetup );

export default Router;