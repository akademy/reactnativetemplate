import { AsyncStorage } from 'react-native'
import { Provider } from "react-redux";
import { persistStore } from 'redux-persist'
import React, { Component } from 'react';

import Router from "./route"
import Store from "./store/store"

import { Console as C } from "./lib/console"

import { stateBase, stateSetup } from "./store/state"

const store = Store(stateBase);

const persisterOptions = {
	blacklist: [
		"errors"
	],
	storage: AsyncStorage
};
const persister = persistStore(store, persisterOptions , () => {
	const state = store.getState();

	if( __DEV__ ) {
		C.groupCollapsed("Rehydration complete");
		C.log("Rehydrated state", state);
		C.groupEnd();
	}

	stateSetup( store );
});

console.log("state", store.getState() );

class App extends Component {
	render() {
		return (
			<Provider store={store}>
				<Router screenProps={ {
					build : this.props.build,
					persister
				} }/>
			</Provider>
		);
	}
}

export default App;

