import uuid from 'react-native-uuid'

import C from "../lib/console";

export const stateBase = {
	dataVersion: 1, // increment if data is incompatible with previous

	randomUuid: null,

	count: 0,
	requestingCount: false,

	currentSomething: null,

	errors: [],
};

export const stateSetup = (store) => {

	if( store.getState().randomUuid === null ) {
		// initial
		store.dispatch( setRandomUuid( generateRandomUuid() ));
	}

	// TODO: Clear errors
	if( store.getState().errors.length !== 0 ) {
		C.log( store.getState().errors );
	}
};

const generateRandomUuid = () => {
	return uuid.v4();
};

const generateRandomId = () => {
	/*
	 Generate an anonymous ID.

	 I'd like it to look simple, and by easier to remember,
	 but still unique without being checked against a server.

	 Removed i, j, L, and q, o as they can look the same.
	 Removed AEU to avoid words being generated.
	 */
	const codeLetters = "BCDFGHKMNPRSTVWXYZ";
	const codeLettersLength = codeLetters.length; // 18
	const codeNumbers = "0123456789";
	const codeNumbersLength = codeNumbers.length; // 10

	let id = "";
	id += codeLetters[Math.floor(Math.random()*codeLettersLength)];
	id += codeNumbers[Math.floor(Math.random()*codeNumbersLength)];
	id += codeNumbers[Math.floor(Math.random()*codeNumbersLength)];
	id += "-";
	id += codeLetters[Math.floor(Math.random()*codeLettersLength)];
	id += codeLetters[Math.floor(Math.random()*codeLettersLength)];
	id += codeLetters[Math.floor(Math.random()*codeLettersLength)];

	return id; // e.g. "M42-GXY", about 10 million combinations
};