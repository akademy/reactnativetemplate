import React, { Component } from 'react';
import { View, Text } from "react-native"

export default class Count extends Component {

	render() {

		return (
			<View>
				<Text
					style={{
						fontSize:28,
						textAlign:"center",
						padding: 20
					}}
				>Count is {this.props.count}</Text>
			</View>
		);
	}
}