
import React, { Component } from 'react';
import { View, Text } from "react-native"

import Template from './template'

export default class About extends Component {
	static navigationOptions = ({ navigation, screenProps }) => ({
		title: "About"
	});

	render() {
		const { navigate } = this.props.navigation;

		return (
			<Template mainTitle="About" subTitle="It's about this">
				<Text>
					This is some text that explains something
				</Text>
			</Template>
		);
	}
}