import React, { Component } from 'react';
import { View, Button, Text } from "react-native"

import Template from './template'

import { NAVIGATION as N } from '../constants'
import { Console as C } from '../console'

import Count from '../components/count/count'

export default class Home extends Component {
	static navigationOptions = ({ navigation, screenProps }) => ({
		title: `${screenProps.build} Home (Home)`
	});

	render() {
		const { navigate } = this.props.navigation;

		return (
			<Template mainTitle="Template" subTitle="It's just an example">
				<View style={{
					//backgroundColor: 'red',
					flex: 1,
					flexDirection: 'column',
					justifyContent: 'space-around'
				}}
				>
					<Count count={this.props.count}/>
					<Button
						title="About"
						onPress={ ()=>{navigate(N.ABOUT)} }
					/>
					<Button
						title="Change Count"
						onPress={ ()=>{navigate(N.COUNT)} }
					/>
					{__DEV__ &&
						<Button
							title="Purge Persist"
							onPress={() => {
								C.warn("Purging the persistant data");
								this.props.screenProps.persister.purge();
							}}
						/>
					}
				</View>
			</Template>
		);
	}
}