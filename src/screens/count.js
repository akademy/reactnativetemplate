
import React, { Component } from 'react';
import { View, Button, Text } from "react-native"

import {Count as CountText} from '../components/count/count'

import Template from './template'
export default class Count extends Component {
	static navigationOptions = ({ navigation, screenProps }) => ({
		title: "Count"
	});

	render() {
		return (
			<Template mainTitle="Count" subTitle="Let's do some counting">
				<Button
					title="Add One"
					onPress={ ()=>{
						if( this.props.onAddOne ) {
							this.props.onAddOne(this.props.count+1);
						}
					} }
				/>
				<Button
					title="Reset"
					onPress={ ()=>{
						if( this.props.onResetCount ) {
							this.props.onResetCount();
						}
					} }
				/>
				<Button
					title="From server"
					onPress={ ()=>{
						if( this.props.onRequestCount ) {
							this.props.onRequestCount();
						}
					} }
				/>
				<CountText count={this.props.count}/>
			</Template>
		);
	}
}